import { Controller, Post } from '@nestjs/common';
import { UsersService } from './users/users.service';
import { User } from './users/user.entity';

@Controller()
export class AppController {
  constructor(private readonly userService: UsersService) {}

  @Post()
  getHello() {
    const users = [
      new User({
        socialSecurityNumber: 'abc',
        specialCode: '123',
        isActive: true,
      }),
      new User({
        socialSecurityNumber: 'def',
        specialCode: '456',
        isActive: true,
      }),
    ];
    return this.userService.createOrUpdate(users);
  }
}
